import { Inject, Injectable } from '@nestjs/common';
import { ClientGrpc } from '@nestjs/microservices';
import { firstValueFrom, from, of, pipe } from 'rxjs';
import { AuthServiceClient, AUTH_SERVICE_NAME, ValidateResponse, TestResponse } from './auth.pb';

@Injectable()
export class AuthService {
  private svc: AuthServiceClient;

  @Inject(AUTH_SERVICE_NAME)
  private readonly client: ClientGrpc;

  public onModuleInit(): void {
    this.svc = this.client.getService<AuthServiceClient>(AUTH_SERVICE_NAME);
  }

  public async validate(token: string): Promise<ValidateResponse> {
    return firstValueFrom(this.svc.validate({ token }));
  }

  public async test({}): Promise<TestResponse> {
    // return firstValueFrom(of({res:"ahmad"}));
    return firstValueFrom(this.svc.test({ }));
  }
}
