import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { EnvService } from './env.service';

const config = new EnvService().read();
async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors(); // Enable CORS for all routes
  await app.listen(config.APP_PORT || 3000, 'localhost');
  console.log(`Gateway Application running at ${await app.getUrl()}`);
}
bootstrap();
