import * as dotenv from 'dotenv'
import * as fs from 'fs'

export interface EnvData {

  APP_PORT:number
  // application
  APP_ENV: string
  APP_DEBUG: boolean


  AUTH_HOST:string
  AUTH_PORT:number


  ORDER_HOST:string
  ORDER_PORT:number


  PRODUCT_HOST:string
  PRODUCT_PORT:number



}

export class EnvService {
  private vars: EnvData

  constructor () {
    const environment = process.env.NODE_ENV || 'development'

    console.log('environment',process.env.NODE_ENV);
   const data: any = dotenv.parse(
      fs.readFileSync(`./../shared-env/${environment}.env`)
    )


    
    data.APP_ENV = environment
    data.APP_DEBUG = data.APP_DEBUG === 'true' ? true : false

    this.vars = data as EnvData
  }

  read (): EnvData {
    return this.vars
  }

  isDev (): boolean {
    return (this.vars.APP_ENV === 'development')
  }

  isProd (): boolean {
    return (this.vars.APP_ENV === 'production')
  }
}