import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { ORDER_SERVICE_NAME, ORDER_PACKAGE_NAME } from './order.pb';
import { OrderController } from './order.controller';
import { EnvService } from './../env.service';

const config = new EnvService().read()
const url = `${config.ORDER_HOST}:${config.ORDER_PORT}`;
@Module({
  imports: [
    ClientsModule.register([
      {
        name: ORDER_SERVICE_NAME,
        transport: Transport.GRPC,
        options: {
          url: url,
          package: ORDER_PACKAGE_NAME,
          protoPath: 'node_modules/grpc-proto/proto/order.proto',
        },
      },
    ]),
  ],
  controllers: [OrderController],
})
export class OrderModule {}
