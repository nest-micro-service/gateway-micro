import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { PRODUCT_PACKAGE_NAME, PRODUCT_SERVICE_NAME } from './product.pb';
import { ProductController } from './product.controller';
import { EnvService } from './../env.service';

const config = new EnvService().read()
const url = `${config.PRODUCT_HOST}:${config.PRODUCT_PORT}`;
@Module({
  imports: [
    ClientsModule.register([
      {
        name: PRODUCT_SERVICE_NAME,
        transport: Transport.GRPC,
        options: {
          url: url,
          package: PRODUCT_PACKAGE_NAME,
          protoPath: 'node_modules/grpc-proto/proto/product.proto',
        },
      },
    ]),
  ],
  controllers: [ProductController],
})
export class ProductModule {}
