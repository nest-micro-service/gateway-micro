## Description
  
Microservices with TypeScript, gRPC, API Gateway, and Authentication | by Ahmad Alhourani

## Repositories

- https://gitlab.com/nest-micro-service/micro_service_boilerplate - App Boilerplate

- https://gitlab.com/nest-micro-service/product-micro - Product Micro (gRPC)
- https://gitlab.com/nest-micro-service/order-micro - Order Micro (gRPC)
- https://gitlab.com/nest-micro-service/auth-micro  - Authentication Micro (gRPC)

- https://gitlab.com/nest-micro-service/gateway-micro - API Gateway (HTTP)
- https://gitlab.com/nest-micro-service/grpc-proto - Shared Proto Repository

## Installation

```bash
sudo snap install protobuf  --classic
sudo apt install -y protobuf-compiler

$ npm install
$ npm run proto:install
$ npm run proto:all
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Author

- Ahmad Alhourani
